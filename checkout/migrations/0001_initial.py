# Generated by Django 3.2.12 on 2023-01-26 20:16

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='order',
            fields=[
                ('id', models.CharField(max_length=200, primary_key=True, serialize=False)),
                ('cart', models.CharField(default='None', max_length=200)),
                ('payment_id', models.CharField(default='None', max_length=200)),
                ('user_id', models.CharField(default='None', max_length=200)),
                ('first_name', models.CharField(default='None', max_length=200)),
                ('last_name', models.CharField(default='None', max_length=200)),
                ('email', models.CharField(default='None', max_length=300)),
                ('address', models.CharField(default='None', max_length=400)),
                ('country', models.CharField(default='None', max_length=50)),
                ('state', models.CharField(default='None', max_length=50)),
                ('city', models.CharField(default='None', max_length=50)),
                ('total', models.FloatField(default=0)),
                ('status', models.CharField(default='None', max_length=30)),
                ('is_ordered', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(default=datetime.datetime(2023, 1, 26, 20, 16, 42, 507122, tzinfo=utc))),
                ('updatd_date', models.DateTimeField(default=datetime.datetime(2023, 1, 26, 20, 16, 42, 507122, tzinfo=utc))),
            ],
        ),
    ]
