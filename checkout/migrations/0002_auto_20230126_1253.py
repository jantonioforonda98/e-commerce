# Generated by Django 3.2.12 on 2023-01-26 20:53

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2023, 1, 26, 20, 53, 21, 184162, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='order',
            name='updatd_date',
            field=models.DateTimeField(default=datetime.datetime(2023, 1, 26, 20, 53, 21, 184162, tzinfo=utc)),
        ),
    ]
